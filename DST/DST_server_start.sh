#!/bin/bash
serveur=$1
reset=$2

source $(dirname $0)/game_globals
[ ! -r "$(dirname $0)/../../params/${GAME_NAME}.params" ] && echo "Fichier de parameteres manquant : $(dirname $0)/../../params/${GAME_NAME}.params" && exit 2
source $(dirname $0)/../../params/${GAME_NAME}.params

[ -z ${serveur} ] && echo "XXX Parametre manquant : nom du serveur XXX" && exit 1
[ ${serveur} == "-h" ] && echo "Liste des serveurs :" && echo "$(ls ~steamcmd/.klei/DoNotStarveTogether/)" && exit 0
[ ${serveur} == "-reset" ] && echo "XXX Parametre manquant : nom du serveur XXX" && exit 2
[ ! -z ${reset} ] && [ ${reset} != "-reset" ] && echo "Le deuxieme parametre doit etre -reset ou rien" && exit 3
echo
echo
echo "############################################"
echo "#                                          #"
echo "#        Demarrage des serveurs DST        #"
echo "#                                          #"
echo "############################################"
echo
echo "  Serveur ${serveur}"
echo
[ ! -z ${reset} ] && [ ${reset} == "-reset" ] && echo "Remise a zero du serveur." && rm -rf ~steamcmd/.klei/DoNotStarveTogether/${serveur}Cluster/Master/save/* && rm -rf ~steamcmd/.klei/DoNotStarveTogether/${serveur}Cluster/Caves/save/*
echo
echo "GAME_PATH : ${GAME_PATH}"
echo
cd ${GAME_PATH}/bin
echo
echo "  --- Demarrage du shard Don't Starve Together, Master "
$(dirname $0)/DST_shard_start.sh ${serveur} Master
sleep 15
echo
echo "  --- Demarrage du shard Don't Starve Together, Caves "
$(dirname $0)/DST_shard_start.sh ${serveur} Caves
echo
echo "Serveur demarre"
